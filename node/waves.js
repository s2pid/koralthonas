var _ = require('lodash');

var possibleActors = [
    {type : 1, howmuch : 10, sides : ['W'], howoften: 10000},
    {type : 1, howmuch : 2, sides : ['E'], howoften: 5000},
    {type : 1, howmuch : 10, sides : ['N'], howoften: 2000},
    {type : 1, howmuch : 5, sides : ['S'], howoften: 5000},
    {type : 1, howmuch : 10, sides : ['W', 'S'], howoften: 5000},
    {type : 1, howmuch : 15, sides : ['E', 'N'], howoften: 5000},
    {type : 1, howmuch : 10, sides : ['W', 'S'], howoften: 4000},
    {type : 1, howmuch : 5, sides : ['W', 'N'], howoften: 5000},
    {type : 1, howmuch : 10, sides : ['E', 'S'], howoften: 2000},
    {type : 1, howmuch : 5, sides : ['N', 'N'], howoften: 5000},
    {type : 1, howmuch : 5, sides : ['W', 'E'], howoften: 5000},
    {type : 1, howmuch : 5, sides : ['N', 'N'], howoften: 5000},
    {type : 1, howmuch : 5, sides : ['W', 'S'], howoften: 5000},
    {type : 1, howmuch : 5, sides : ['W', 'E'], howoften: 5000},
    {type : 1, howmuch : 5, sides : ['E', 'N', 'E'], howoften: 5000},
    {type : 1, howmuch : 5, sides : ['W', 'S'], howoften: 5000},
    {type : 1, howmuch : 5, sides : ['W', 'S', 'N'], howoften: 5000},
    {type : 1, howmuch : 5, sides : ['E', 'S', 'N'], howoften: 6000},
    {type : 1, howmuch : 10, sides : ['W', 'S', 'N', 'E'], howoften: 6000},
    {type : 1, howmuch : 10, sides : ['W', 'S', 'N', 'E'], howoften: 3000}
];

var template = {
    actors : [

    ],
    timeout : 5000
};

var possibleSides = ['W', 'E', 'N', 'S'];

exports.generateWave = function( fold ) {
    var wave = _.clone(template);
    var actors = _.shuffle(possibleActors);
    var rand = (fold < 5) ? fold : _.random(5, fold+1);
    wave.actors = _.sample(actors, rand);
    console.log(wave);
    return wave;
}