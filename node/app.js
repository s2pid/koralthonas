var express     = require('express.io'),
    waves       = require('./waves.js'),
    low         = require('lowdb'),
    path        = require('path')

var app = express().http().io()

app.use(express.static(__dirname + '/public'));
app.use('/node_modules',  express.static(__dirname + '/node_modules'));
app.use('/css',  express.static(__dirname + '/../css'));
app.use('/js',  express.static(__dirname + '/../js'));
app.use('/assets',  express.static(__dirname + '/../assets'));

var db = low('db.json');

app.get('/', function(req, res) {
    res.sendfile(path.resolve(__dirname + '/../index.html'))
});

app.get('/client', function(req, res) {
    res.sendfile(__dirname + '/client.html')
});

app.io.route('startGame', function(req) {
    req.io.emit('accepted');
    req.io.emit('newWave', {'wave' : waves.generateWave(1) });
});

app.io.route('waveCleared', function(req) {
    req.io.emit('newWave', {'wave' : waves.generateWave(req.data) });
});

app.io.route('fucked', function(req) {
    db('score').push(req.data);
    var res = db('score').chain().sortBy('score').reverse().take(10).value();
    app.io.broadcast('returnedResults', {'results' : res });
    req.io.emit('fuckedBack');
});

app.io.route('results', function(req) {
    var res = db('score').chain().sortBy('score').reverse().take(10).value();
    req.io.emit('returnedResults', {'results' : res });
});

app.listen(7076)