var bulletType = [
    {
        damage:1,
        speed:1000
    },
    {
        damage: 5,
        speed: 900
    }
];

var Bullet = function(type) {
    this.damage = bulletType[type].damage;
    this.speed = bulletType[type].speed;
    this.node = '';
};

Bullet.prototype.assign = function (x,y) {
    var $this = this;

    $this.node = $('<div id="'+Math.floor((Math.random() * 100) + 1)+'" class="bullet"></div>');
    $this.node.css({
        left:x+'px',
        top:y+'px'
    });
    $(document).find('#main-wrap').append($this.node);
};

Bullet.prototype.move = function (degree) {
    var $this = this;

    if (degree > 315 || degree <= 45) {

        if(degree > 315){
            var multyplyer = degree - 315;
        }
        if(degree < 45){
            var multyplyer = 45+degree;
        }
        var x = multyplyer * 8.9;
        var y = 0;


    } else if (degree > 45 && degree <= 135) {

            var multyplyer = degree-45;
            var x = 800;
            var y = multyplyer*5;

    } else if (degree > 135 && degree <= 225) {
        var multyplyer = degree-135;
        var x = 800-multyplyer*8.9;
        var y = 450;

    } else if (degree > 225 && degree <= 315) {
        var multyplyer = degree-225;
        var x = 0;
        var y = 450-multyplyer*5;
    }


    $this.node.css({
        rotate: degree+'deg'
    });

    $this.node.animate({
        top: y+'px',
        left: x+'px'
    }, $this.speed, function() {
        $this.delete();
    });
};

Bullet.prototype.delete = function (degree) {
    this.node.remove();
    delete this;
};