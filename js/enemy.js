var CENTER_X = $('#main-wrap').width() / 2 - 5;
var CENTER_Y = $('#main-wrap').height() / 2 - 5;

var ENEMY_TYPES = {
    1: {
        health: 1,
        size: 30,
        speed: 5000
    }
};

var Enemy = function(side, game, type) {
    this.health = ENEMY_TYPES[type].health;
    this.size = ENEMY_TYPES[type].size;
    this.DOMElement = '';
    this.side = side;
    this.game = game;
    this.speed = ENEMY_TYPES[type].speed;

    this.createAt(side);
};


Enemy.prototype.createAt = function (side) {

    var x, y,rotate;
    switch(side) {
        case 'E':
            x = 800;
            y = 225;
            rotate = 270;
            break;
        case 'W':
            x = 0;
            y = 225;
            rotate = 90;
            break;
        case 'S':
            x = 400;
            y = 450;
            rotate = 0;
            break;
        case 'N':
            x = 400;
            y = 0;
            rotate = 180;
            break;

    }

    var $this = this;
    var object;
    object = $('<div id="'+Math.floor((Math.random() * 100) + 1)+'" class="enemy"><div class="body"></div></div>');

    object.css({
        left:x+'px',
        top:y+'px',
        width:$this.size+'px',
        height:$this.size+'px',
        rotate: rotate+'deg'
    });
    $this.DOMElement = object;
    console.log($this);
    //$this.game.map.append(object)
    $this.game.map.append(object)
};

Enemy.prototype.move = function (x, y) {
    var $this = this;
    var object = $this.DOMElement;
    object.animate({
        top: CENTER_Y-($this.size / 2)+'px',
        left: CENTER_X-($this.size / 2)+"px"
    }, $this.speed, 'linear');
    //object.transition({ translate: [x,y] }, 500, 'in');

};
//var enemy = new Enemy(1, $('#main-wrap'), 1);
//enemy.createAt('W');
//enemy.move(400,225);