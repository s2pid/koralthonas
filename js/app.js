// load this file last
$(function () {

    var game = new Game(socket, 'main-wrap');

    //var name = prompt('Enter your name');
    var name = 'julius';


    $('#splash').fadeOut(3000, function(){
        game.srv.emit('startGame', {name: name}, function (data) {
        });

        game.srv.on('accepted', function (data) {
            game.start();
            soundZombie = new buzz.sound("assets/zombie.wav");
            soundZombie.loop().play().fadeIn();
        });
    });


    //var template = {
    //    actors : [
    //        {type : 1, howmuch : 10, sides : ['W', 'S', 'E', 'N'], howoften: 500},
    //        {type : 1, howmuch : 5, sides : ['E'], howoften: 1000},
    //        {type : 1, howmuch : 2, sides : ['N'], howoften: 500},
    //        {type : 1, howmuch : 2, sides : ['S'], howoften: 500}
    //    ],
    //    timeout : 5000
    //};

    var newWave = function (data) {
        data = data.wave;
        var actors = data.actors,
            timeout = data.timeout,
            fold = data.fold;

        game.incWaveNo();

        game.spawnEnemies(actors, timeout, fold);
        /**
         * var template = {
            actors : [
                {type : 1, howmuch : 10, sides : ['W'], howoften: 500},
                {type : 1, howmuch : 5, sides : ['E'], howoften: 1000},
                {type : 1, howmuch : 2, sides : ['N'], howoften: 500},
                {type : 1, howmuch : 2, sides : ['S'], howoften: 500}
            ],
            timeout : 5000
        };
         */
    };

    //newWave(template);

    game.srv.on('newWave', newWave);

    game.srv.on('returnedResults', function(data){
        var cont = $('#scoreboard .scores ol');
        cont.empty();
        for(var i = 0, len = data.results.length; i < len; i++){
            cont.append($('<li/>').html(data.results[i].name+' - '+data.results[i].score));
        }
    });

    game.srv.emit('results');

    var volume = 30;
    soundShot = new buzz.sound("assets/shot.ogg");
    buzz.all().setVolume(volume);

    $('a.sound').click(function(){
       var that = $(this);
       if(that.hasClass('off')){
           soundZombie.play();
           soundShot.unmute();
           that.removeClass('off');
       }else{
           soundZombie.stop();
           soundShot.mute(0);
           that.addClass('off');
       }
    });
    // Stop after 1 second
/*
    setTimeout(function () {
        game.stop();
    }, 1000)
*/
});