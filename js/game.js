var Game = function (serverSocket, mapNode) {
    this.map = $('#' + mapNode);
    this.waveNo = 0;
    this.fps = 30;
    this.tick = 0;
    this.startTime = new Date();
    this.hero = null;
    this.enemies = [];
    this.projectiles = [];
    this.srv = serverSocket;
    this.gameLoop = null;
    this.points = 0;
    this.width = this.map.width();
    this.height = this.map.height();
};

Game.prototype.waveCleared = function () {
    this.srv.emit('waveCleared', this.waveNo);
};

Game.prototype.start = function () {
    var interval = 1000 / this.fps;

    this.waveNo = 0;
    this.points = 0;

    this.hero = new Hero(this);
    this.hero.init();

    var _this = this;
    this.gameLoop = setInterval(function () {
        _this.runCycle();
        _this.tick++;
    }, interval);
};

Game.prototype.stop = function () {
    clearTimeout(this.gameLoop);
};

Game.prototype.getUpTime = function () {
    var now = new Date();
    return now - this.startTime;
};

Game.prototype.incWaveNo = function () {
    this.waveNo++;
};

Game.prototype.getSrv = function () {
    return this.srv;
};
function intersectRect(r1, r2) {

    var maxAx = r1.left + r1.width,
        minBx = r2.left,
        minAx = r1.left,
        maxBx = r2.left + r2.width;

    var maxAy = r1.top + r1.height,
        minBy = r2.top,
        minAy = r1.top,
        maxBy = r2.top + r2.height;


    var aLeftOfB = maxAx < minBx,
        aRightOfB = minAx > maxBx,
        aAboveB = minAy > maxBy,
        aBelowB = maxAy < minBy;
    var intersects = !( aLeftOfB || aRightOfB || aAboveB || aBelowB );

    if (intersects) {
        console.log('intersects');
        return true;
    } else {
        return false;
    }


    //return !( aLeftOfB || aRightOfB || aAboveB || aBelowB );
}

Game.prototype.checkCollisions  = function () {

    var newProjectiles = [];
    for (var x = 0; x < this.projectiles.length; x++) {
        var projectile = this.projectiles[x];
        //if (!projectile.node) {
        //    //continue;
        //} else {
        //    newProjectiles.push(projectile);
        //}
        var node = projectile.node,
            width = node.width(),
            height = node.height(),
            position = node.position(),
            xPos = position.left + (width / 2),
            yPos = position.left + (height / 2);

        var projectileRect = {
            left: xPos,
            top: yPos,
            width: width,
            height: height
        };




        var newEnemies = [];
        var deleted = false;
        for (var y = 0; y < this.enemies.length; y++) {
            var enemy = this.enemies[y];

            var enemyNode = enemy.DOMElement,
                enemyWidth = enemyNode.width(),
                enemyHeight = enemyNode.height(),
                enemyPosition = enemyNode.position(),
                enemyXPos = enemyPosition.left + (enemyWidth / 2),
                enemyYPos = enemyPosition.left + (enemyHeight / 2);

            var enemyRect = {
                left: enemyXPos,
                top: enemyYPos,
                width: enemyWidth,
                height: enemyHeight
            };

            var intersects = intersectRect(projectileRect, enemyRect);
            if (intersects) {
                projectile.node.remove();
                enemy.DOMElement.remove();
                delete this.projectiles[x];
                deleted = true;
            } else {
                newEnemies.push(enemy);
            }
        }
        this.enemies = newEnemies;


        if (!deleted) {
            newProjectiles.push(projectile);
        }
    }
    this.projectiles = newProjectiles;
};

Game.prototype.runCycle = function () {
    this.checkCollisions();
    // check positions of enemies,
    // check positions of projectiles (raketos)
        // Jei pozicijos overlapina, vadinasi enemy nusautas
};

Game.prototype.gameOver = function () {
    this.srv.emit('fucked', {name: this.name, score: this.score});
};

Game.prototype.spawnEnemies = function (actors, timeout, fold) {
    var _this = this;
    var spawnAfter = {val: 0};
    for (var x = 0; x < actors.length; x++) {
        spawnAfter.val += Math.floor(Math.random() * 6) + 1;

        var actor = actors[x];
        for (var current = 0; current < actor.howmuch; current++) {

            (function (actor, spawnAfter) {
                setTimeout(function () {
                    var randomSide = Math.floor(Math.random() * actor.sides.length),
                    enemy = new Enemy(actor.sides[randomSide], _this, actor.type);
                        //enemy = new Object(actor.sides[randomSide], _this, actor.type);
                        enemy.move();
                    _this.enemies.push(enemy);
                }, spawnAfter.val);
            })(actor, spawnAfter);
            spawnAfter.val += actor.howoften;
        }
    }
};

Game.prototype.addBullet = function (bullet) {
    this.projectiles.push(bullet);
};