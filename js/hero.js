var CENTER_X = $('#main-wrap').width() / 2 - 15;
var CENTER_Y = $('#main-wrap').height() / 2 - 15;


var Hero = function(game) {
    this.game = game;
    this.name = '';
    this.health = 100;
    this.armor = 100;
    this.node = '';
    this.degree = 3;
    this.bulletType = 0;
};

Hero.prototype.fire = function () {
    var $this = this;
    document.onkeypress = function(e) {
        if (e.keyCode == 32) {
            var bullet = new Bullet($this.bulletType);
            bullet.assign(CENTER_X, CENTER_Y);
            var degree = getRotationDegrees($this.node);
            bullet.move(degree);

            $this.game.addBullet(bullet);
            soundShot.stop();
            soundShot.play();

            return false;
        }
    }
};


Hero.prototype.assign = function (x, y) {
    var $this = this;
    $this.node = $('<div id="'+Math.floor((Math.random() * 100) + 1)+'" class="hero"><div class="body"></div><div class="gun"></div></div>');
    $this.node.css({
        left:x+'px',
        top:y+'px'
    });

    $('#main-wrap').append($this.node);
};


Hero.prototype.rotate = function () {
    var $this = this;

    for (var direction in keys) {
        if (!keys.hasOwnProperty(direction)) continue;
        if (direction == 37) {
            $this.node.css({ rotate: '-='+$this.degree+'' });
        }
        if (direction == 39) {
            $this.node.css({ rotate: '+='+$this.degree+'' });
        }
    }
}


var keys = {};

$(document).keydown(function(e) {
    keys[e.keyCode] = true;
});

$(document).keyup(function(e) {
    delete keys[e.keyCode];
});






Hero.prototype.init = function () {
    $this = this;
    console.log($this);
    $this.assign(CENTER_X, CENTER_Y);

    var actions = function() {
        $this.rotate();

    }
    setInterval(actions, 20);
    $this.fire();
};















function getRotationDegrees(obj) {
    var matrix = obj.css("-webkit-transform") ||
        obj.css("-moz-transform")    ||
        obj.css("-ms-transform")     ||
        obj.css("-o-transform")      ||
        obj.css("transform");
    if(matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];
        var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
    } else { var angle = 0; }
    return (angle < 0) ? angle + 360 : angle;
}